FROM debian:buster
CMD ["/bin/bash"]
RUN export DEBIAN_FRONTEND=noninteractive; \
    apt-get update; \
    apt-get install -y curl wget unzip; \
    cd /tmp; \
    curl -LO https://storage.googleapis.com/kubernetes-release/release/v1.18.2/bin/linux/amd64/kubectl \
        && install kubectl /usr/local/bin/; \
    curl -LO https://github.com/derailed/k9s/releases/download/v0.19.6/k9s_Linux_x86_64.tar.gz \
        && tar -zxvf k9s_Linux_x86_64.tar.gz \
        && install k9s /usr/local/bin/; \
    curl -LO https://github.com/yudai/gotty/releases/download/v1.0.1/gotty_linux_amd64.tar.gz \
        && tar -zxvf gotty_linux_amd64.tar.gz \
        && install gotty /usr/local/bin/; \
    curl -LO https://github.com/tsl0922/ttyd/releases/download/1.6.0/ttyd_linux.x86_64 \
        && install ttyd_linux.x86_64 /usr/local/bin/ttyd; \
    curl -LO https://d1vvhvl2y92vvt.cloudfront.net/awscli-exe-linux-x86_64.zip \
        && unzip awscli-exe-linux-x86_64.zip \
        && ./aws/install; \
    apt-get install -y \
        jq tmux htop procps \
        vim \
        dnsutils apache2-utils \
        mycli postgresql-client redis-tools \
        openssh-client; \
    rm -rf /tmp/*; \
    rm -rf /var/lib/apt/lists
