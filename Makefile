build:
	docker build -t swissknife .

run: build
	docker run --rm -it -p 8080:80 swissknife ttyd -p 80 -c test:test tmux new-session -A -s debug
